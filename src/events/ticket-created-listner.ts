import { Message } from 'node-nats-streaming';
import ListnerBase from './base-listner';
import { TicketCreatedEvent } from './ticket-created-event';
import { Subjects } from './subjects';

class TicketCreatedListner extends ListnerBase<TicketCreatedEvent> {
  readonly subject = Subjects.TicketCreated;
  queueGroupName = 'payments-service';
  onMessage(data: TicketCreatedEvent['data'], msg: Message) {
    console.log(data.id)
    console.log(data.price)
    console.log(data.title)
    msg.ack();
  }
}

export default TicketCreatedListner;
