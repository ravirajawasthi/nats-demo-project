import nats from 'node-nats-streaming';
import { TicketCreatedPublisher } from './events/ticket-created-publisher';
import TicketCreatedListner from './events/ticket-created-listner';
console.clear();
const stan = nats.connect('ticketing', 'abc', {
  url: 'http://localhost:4222',
});

stan.on('connect', async () => {
  console.log('connected to nats-streaming-server');
  const publisher = new TicketCreatedPublisher(stan)
  await publisher.publish({
    id: "090u0kopjdf",
    title: "Concert",
    price: 100
  })
});
